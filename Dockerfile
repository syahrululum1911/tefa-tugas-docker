FROM node:16

COPY main.js /app/main.js

CMD ["node", "/app/main.js"]